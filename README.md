# Remote FTP Deploy

An application for remote upload directory's files via FTP

### Installation

It requires [Node.js](https://nodejs.org/) < v11 to run.

Running with NodeJs

```sh
$ cd folder_name
$ npm install 
$ node index
```

Production

```sh
$ npm install
$ pkg .
```

### Plugins

| Plugin | Link |
| ------ | ------ |
| Fs | https://nodejs.org/api/fs.html |
| prompt | https://www.npmjs.com/package/prompt |
| ftp-deploy | https://www.npmjs.com/package/ftp-deploy |
| pkg | https://github.com/zeit/pkg |


### Todos

 - Install node & npm
 - npm install -g pkg

License
----

MIT
