const fs = require('fs');
const prompt = require('prompt');
const FtpDeploy = require('ftp-deploy');
const ftpDeploy = new FtpDeploy();
const begin = {
    properties: {
        action: {
            message: 'Saved Config[S], New Config[N], Quit[Q]',
            required: true,
        },
    },
};
const load = {
    properties: {
        index: {
            message: 'Select the saved config',
            description: 'Select the saved config',
            type: 'number',
            required: true,
        },
    },
};
const schema = {
    properties: {
        user: {
            message: 'Username',
            required: true,
        },
        password: {
            message: 'Password',
            required: true,
            hidden: true,
        },
        host: {
            message: 'Hostname',
            required: true,
        },
        localRoot: {
            message: 'LocalRoot',
            required: true,
        },
        remoteRoot: {
            message: 'RemoteRoot',
            default: '/httpdocs/',
            required: true,
        }
    },
};
const confirm = {
    properties: {
        confirm: {
            message: 'Confirm[C], Save[S], Reset[R], Quit[Q]',
            required: true,
        },
    },
};

prompt.start();
const cache = (obj, key = '') => {
    if (fs.existsSync('./cache')) {
        if (fs.existsSync('./cache/temp.json')) {
            let data = fs.readFileSync('./cache/temp.json','UTF-8');
            data = JSON.parse(data);
            data[key] = obj;
            fs.writeFileSync('./cache/temp.json', JSON.stringify(data));
        } else {
            fs.writeFileSync('./cache/temp.json', JSON.stringify(temp));
        }
    } else {
        let temp = {};
        temp[key] = obj;
        fs.mkdirSync('./cache', 0777);
        fs.writeFileSync('./cache/temp.json', JSON.stringify(temp));
    } console.log(`Saved: ${key}`);
};
const upload = (obj, temp = false) => {
    const config = {
        user: obj.user,
        password: obj.password,
        host: obj.host,
        port: 21,
        localRoot: obj.localRoot.trim(),
        remoteRoot: obj.remoteRoot.trim(),
        include: ['*', '**/*'],              // this would upload everything except dot files
        // include: ['*.php', 'dist/*'],
        exclude: ['dist/**/*.map'],          // e.g. exclude sourcemaps - ** exclude: [] if nothing to exclude **
        deleteRemote: false,                 // delete ALL existing files at destination before uploading, if true
        forcePasv: true                      // Passive mode is forced (EPSV command is not sent)
    }
    return ftpDeploy.deploy(config)
        .then(res => {
            if (temp) cache(config, `${obj.user}@${obj.host}`);
            if (res.length > 0) {
                let count = 0;
                res.map((x, i) => {
                    x.map((y, j) => console.log(`${j + count + 1}. ${y}`));
                    count += x.length;
                })
            }
        })
        .catch(err => console.log(err))
};
const input = (temp = {}) => {
    if (Object.keys(temp).length === 0) {
        prompt.get(schema, function (err, res) {
            console.log('----------------------------------------------');
            console.log('Please confirm:');
            console.log(`Username: ${res.user}`);
            console.log(`Password: ${res.password}`);
            console.log(`Hostname: ${res.host}`);
            console.log(`LocalRoot: ${res.localRoot}`);
            console.log(`LocalRoot: ${res.remoteRoot}`);
            console.log('----------------------------------------------');
            prompt.get(confirm, function (err, ress) {
                if ((ress.confirm === 's' || ress.confirm === 'S')) {
                    return upload(res, true);
                } else if (ress.confirm === 'c' || ress.confirm === 'C') {
                    return upload(res);
                } else if (ress.confirm === 'r' || ress.confirm === 'R') {
                    return input();
                } else {
                    return;
                }
            });
        });
    } else {
        const newConfirm = Object.assign(confirm);
        newConfirm.properties.confirm.message = 'Confirm[C], Quit[Q]';
        prompt.get(confirm, function (err, ress) {
            if (ress.confirm === 'c' || ress.confirm === 'C') {
                return upload(temp);
            } else {
                return;
            }
        });
    }
};
const list = () => {
    if (fs.existsSync('./cache/temp.json')) {
        let temp = [];
        let data = fs.readFileSync('./cache/temp.json','UTF-8');
        data = JSON.parse(data);
        const array = Object.keys(data);
        if (array.length > 0) {
            array.map((x, i) => {
                temp.push(x);
                console.log(`- [${i}] ${x}`);
            });
            console.log(`- [${array.length}] Remove all record(s)`);
            console.log(`- [${array.length + 1}] Go back`);
            prompt.get(load, function (err, res) {
                if (res.index >= 0 && res.index < array.length) {
                    return input(data[temp[res.index]]);
                } else if (res.index === array.length) {
                    fs.writeFileSync('./cache/temp.json', JSON.stringify({}));
                    return start();
                } else if (res.index === array.length + 1) {
                    return start();
                } else {
                    return console.log('Failed: Saved record isn\'t exist');
                }
            });
        } else {
            console.log('Failed: No saved config.')
            return start();
        }
    } else {
        console.log('Failed: No saved config.')
        return start();
    }
};
const start = () => {
    prompt.get(begin, function (err, res) {
        if ((res.action === 's' || res.action === 'S')) {
            return list();
        } else if (res.action === 'n' || res.action === 'N') {
            return input();
        } else {
            return;
        }
    });       
};

start();

